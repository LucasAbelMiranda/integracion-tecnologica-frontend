import { TestBed } from '@angular/core/testing';

import { SaludServiceService } from './salud-service.service';

describe('SaludServiceService', () => {
  let service: SaludServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaludServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
