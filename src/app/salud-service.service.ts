import { Injectable } from '@angular/core';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { dataSaludI } from './models/dataSalud.interface';

@Injectable({
  providedIn: 'root'
})
export class SaludServiceService {

  baseURL: string = "http://localhost:3000";

  constructor(private http: HttpClient) { }

  getInfo(): Observable<dataSaludI[]> {
   return this.http.get<dataSaludI[]>('/api/getInfo')
  }


}
