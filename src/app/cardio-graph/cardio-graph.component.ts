import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from "HighCharts";

@Component({
  selector: 'app-cardio-graph',
  templateUrl: './cardio-graph.component.html',
  styleUrls: ['./cardio-graph.component.css']
})
export class CardioGraphComponent implements OnInit {

  //@Input() inputData: any = []; 

  private _inputData: any = [];

  @Input() set inputData(value: any) {

    this._inputData = value.cardioArray;
    console.log("hola ", value.cardioArray);
    this.options.series[0].data = value.cardioArray;
    Highcharts.chart('container', this.options);
  }

  public options: any = {
    chart: {
      type: 'line',
      height: 300
    },
    title: {
      text: 'ultimas mediciones'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      type: 'Linear',
    },
    series: [
      {
        name: 'pulsaciones',
        turboThreshold: 400,
        data: this._inputData,
        marked: false
      }
    ],


  }

  constructor() { }




  ngOnInit(): void {


    Highcharts.chart('container', this.options);

  }

  ngOnChanges(): void {
    Highcharts.chart('container', this.options);
  }
}
