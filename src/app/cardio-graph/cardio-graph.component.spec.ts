import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardioGraphComponent } from './cardio-graph.component';

describe('CardioGraphComponent', () => {
  let component: CardioGraphComponent;
  let fixture: ComponentFixture<CardioGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardioGraphComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardioGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
