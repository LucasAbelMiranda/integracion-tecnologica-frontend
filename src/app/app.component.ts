import { Component } from '@angular/core';

import { SaludServiceService } from './salud-service.service'

import { dataSaludI } from './models/dataSalud.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {

  infoSalud: dataSaludI[] = [];
  timeIntevalSeconds = 5;
  cardioArray: number[] = [];

  cardioTemp = 0;

  constructor(private saludServiceService: SaludServiceService) {
  }

  getInfo() {
    this.saludServiceService.getInfo().subscribe(data => this.processData(data))

  }

  processData(data: dataSaludI[]) {

    this.infoSalud = data;
    if (data) {
      this.cardioArray = data[0].cardio.split(',').map(function (item) {
        return parseInt(item, 10);
      });

      this.cardioArray = this.cardioArray.filter(function (value) {
        return !Number.isNaN(value);
      });

      this.cardioTemp = this.cardioArray.reduce((a, b) => a + b, 0) / this.cardioArray.length;

      console.log(this.cardioArray);
    }
  }

  ngOnInit() {
    this.saludServiceService.getInfo().subscribe(data => this.infoSalud = data)
    setInterval(() => { this.getInfo() }, this.timeIntevalSeconds * 1000);
  }

  title = 'front-integracion';





}
